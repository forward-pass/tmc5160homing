/* TMC5160 SPI example

  This code demonstrates the usage of a Trinamic TMC5160 stepper driver in SPI mode.

  Hardware setup :
  Connect the following lines between the microcontroller board and the TMC5160 driver
  (Tested with a Teensy 3.2 and a TMC5160-BOB)

  MOSI (Teensy : 11)  <=> SDI
  MISO (Teensy : 12)  <=> SDO
  SCK (Teensy : 13)   <=> SCK
  10                   <=> CSN
  7                   <=> DRV_ENN (optional, tie to GND if not used)
  GND                 <=> GND
  3.3V/5V             <=> VCC_IO (depending on the processor voltage)

  The TMC5160 VS pin must also be powered.
  Tie CLK16 to GND to use the TMC5160 internal clock.
  Tie SPI_MODE to VCC_IO, SD_MODE to GND.

  Please run the Config Wizard for power stage fine tuning / motor calibration (this code uses the default parameters and auto calibration).

  Copyright (c) 2020 Tom Magnier

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <Arduino.h>
#include <TMC5160.h>

const uint8_t SPI_CS = 5;       // CS pin in SPI mode
const uint8_t SPI_DRV_ENN = 4;  // DRV_ENN pin in SPI mode
//const uint8_t STEP_PIN = 16;  // STEP pin

static bool MOTOR_TEST = false;
static bool VERBOSE = false;
static bool HOME_CAL = false;

int MaxSpeed = 800;             // 1 full rotation = 200s/rev
float MaxPosition = 3E+38;      // Used for homing position
int VPWM = 80;
int VCOOLTHRS = 150;
int VHIHG = 400;
bool homed = false;

TMC5160_SPI motor = TMC5160_SPI(SPI_CS);                  //Use default SPI peripheral and SPI settings.

TMC5160_Reg::GCONF_Register gConf = { 0 };                // 0x00, Global configuration flags
TMC5160_Reg::RAMP_STAT_Register rampStatus = { 0 };       // 0x35, Ramp status and switch event status
TMC5160_Reg::IOIN_Register ioin = { 0 };                  // 0x04, Read input / write output pins
TMC5160_Reg::DRV_STATUS_Register drvStatus = { 0 };       // 0x6F, stallGuard2 value and driver error flags
TMC5160_Reg::SW_MODE_Register swMode = { 0 };             // 0x34, Switch mode configuration
TMC5160_Reg::IHOLD_IRUN_Register iHoldiRun = { 0 };       // 0x10, Driver current control
TMC5160_Reg::COOLCONF_Register coolConf = { 0 };          // 0x6D, coolStep smart current control register and stallGuard2 configuration

// For motor testing purposes
uint32_t dir_change_time    = 10000;
uint32_t print_time         = 100;
float   test_target         = 5000;
float prev_target = motor.getCurrentPosition();

// void IRAM_ATTR isr() {
//     digitalWrite(STEP_PIN, !digitalRead(STEP_PIN));
// }

float move_until_stall(TMC5160_SPI motor, TMC5160_Reg::DRV_STATUS_Register drvStatus,
                       int initial_target, int move_speed = 400, int delay_time = 500,
                       int min_move_treshold = 100, int update_threshold = 500) {
    // Move the motor in a certain direction until the motor is stalled and return current position
    // TMC5160_SPI motor: current motor object
    // TMC5160_Reg::DRV_STATUS_Register drvStatus: the register object, no point in making a new one everytime
    // int initial_target: target position in number of full steps. Will move in the direction of the target position relative to the current position
    // int move_speed: speed of the motor to find the stall
    // int delay_time: wait a certain amount of time before motor commads to allow for the tmc to process
    // int min_move_treshold: minimum movement until it starts checking stall
    // int update_threshold: doubles the target position if current position is within this number of steps close to the previous target position.

    motor.setMaxSpeed(move_speed);

    int temp_pos = initial_target;
    int start_pos = motor.getCurrentPosition();
    motor.setTargetPosition(temp_pos);
    delay(delay_time);

    drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
    // While it is NOT stalled (So keep moving)
    while (!(drvStatus.stallguard ? true : false) ||
            abs(motor.getCurrentPosition() - start_pos) < min_move_treshold) {
        if (motor.getCurrentPosition() - temp_pos < update_threshold) {
            temp_pos *= 2;
            motor.setTargetPosition(temp_pos);
        }
        drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
        Serial.print("StallGuardValue:"); Serial.print(drvStatus.sg_result, DEC); Serial.print(", ");
        Serial.print("StallGuardStatus:"); Serial.print(drvStatus.stallguard ? true : false); Serial.print(", ");
        Serial.println();
    }

    // Stop motor
    motor.setTargetPosition(motor.getCurrentPosition());
    delay(delay_time);

    // Reset stall
    swMode.sg_stop = 0;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
    swMode.sg_stop = 1;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
    delay(delay_time);

    return motor.getCurrentPosition();
}

void setup() {
    //attachInterrupt(STEP_PIN, isr, CHANGE);
    // USB/debug serial coms
    Serial.begin(250000);

    pinMode(SPI_DRV_ENN, OUTPUT);
    digitalWrite(SPI_DRV_ENN, LOW); // Active low

    // This sets the motor & driver parameters /!\ run the configWizard for your driver and motor for fine tuning !
    TMC5160::PowerStageParameters powerStageParams; // defaults.
    TMC5160::MotorParameters motorParams;
    motorParams.globalScaler = 140; // Adapt to your driver and motor (check TMC5160 datasheet - "Selecting sense resistors")
    motorParams.irun = 31;
    motorParams.ihold = 16;

    SPI.begin();
    // Check if the TMC5160 answers back
    Serial.println("press any key.");
    while (!Serial.available());
    while (ioin.version != motor.IC_VERSION) {
        ioin.value = motor.readRegister(TMC5160_Reg::IO_INPUT_OUTPUT);

        if (ioin.value == 0 || ioin.value == 0xFFFFFFFF) {
            Serial.println("No TMC5160 found.");
            delay(2000);
        }
        else {
            Serial.println("Found a TMC device.");
            Serial.print("IC version: 0x");
            Serial.print(ioin.version, HEX);
            Serial.print(" (");
            if (ioin.version == motor.IC_VERSION) {
                Serial.println("TMC5160).");
            }
            else {
                Serial.println("unknown IC !)");
            }
        }
    }

    // General Configuration //

    //gConf.value = motor.readRegister(TMC5160_Reg::GCONF);
    //gConf.direct_mode = 1;
    //motor.writeRegister(TMC5160_Reg::GCONF, gConf.value);

    // Reference Switch Mode Setup //

    swMode.value = motor.readRegister(TMC5160_Reg::SW_MODE);
    swMode.en_softstop = 0;
    swMode.sg_stop = 1;
    swMode.pol_stop_r = 0;
    swMode.pol_stop_l = 0;
    swMode.stop_r_enable = 0;
    swMode.stop_l_enable = 0;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);

    // Driver Current Control //

    //iHoldiRun.value = motor.readRegister(TMC5160_Reg::IHOLD_IRUN);
    //iHoldiRun.ihold = 31;
    //iHoldiRun.irun = 31;
    //iHoldiRun.iholddelay = 3;
    //motor.writeRegister(TMC5160_Reg::IHOLD_IRUN, iHoldiRun.value);

    // Global Scaler Setting //

    //motor.writeRegister(TMC5160_Reg::GLOBAL_SCALER, 0);

    // Smart energy control CoolStep and StallGuard2 //

    coolConf.value = motor.readRegister(TMC5160_Reg::COOLCONF);
    coolConf.sfilt = 0;
    coolConf.sgt = 0; // more sensitive [-64..63] less sensitive
    motor.writeRegister(TMC5160_Reg::COOLCONF, coolConf.value);

    // check SD mode //

    //Serial.print("sdmode: ");
    //Serial.println(ioin.sd_mode ? true : false);

    // ramp definition
    motor.begin(powerStageParams, motorParams, TMC5160::NORMAL_MOTOR_DIRECTION);

    motor.setRampMode(TMC5160::POSITIONING_MODE);
    motor.setCurrentPosition(0);
    motor.setMaxSpeed(MaxSpeed);
    motor.setRampSpeeds(0, 200, 200);
    motor.setAccelerations(1000, 1200, 2000, 2000); //motor.setAccelerations(25, 70, 100, 100);
    motor.setModeChangeSpeeds(VPWM, VCOOLTHRS, VHIHG);


    Serial.println("starting up");
    print_help();

    delay(1000); // Standstill for automatic tuning
}

void print_help() {
    Serial.println("'c' to calibrate motors");
    Serial.println("'t' to test motors");
    Serial.println("'p' increase motor speed by 20");
    Serial.println("'m' decrease motor speed by 20");
    Serial.println("'r' reset stall");
    Serial.println("'h' display this help");
    Serial.println();
}

void loop() {
    uint32_t now = millis();
    static unsigned long t_dirchange, t_echo;
    static int32_t target;
    static bool dir;
    /*
        Serial.println((drvStatus.stallguard ? true:false) & homed);
        drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
        if ((drvStatus.stallguard ? true:false) & homed){
        motor.setTargetPosition(motor.getCurrentPosition());
        swMode.sg_stop = 0;
        motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
        swMode.sg_stop = 1;
        motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
        delay(500)
        if(prev_target < target){
        if(motor.getCurrentPosition()-300>0){
            motor.setTargetPosition(motor.getCurrentPosition()-300);
        }
        else{
            motor.setTargetPosition(0);
        }
        }
        else{
            if(motor.getCurrentPosition()+300<MaxPosition){
                motor.setTargetPosition(motor.getCurrentPosition()+300);
            }
            else{
                motor.setTargetPosition(MaxPosition);
            }
            }
        }
    */
    if (Serial.available() > 0) {
        String input = Serial.readStringUntil('\n');
        if (input == "") {
            input = "0";
        }
        if (input == "h") {
            print_help();
        }
        else if (input == "p") {
            MaxSpeed += 20;
            motor.setMaxSpeed(MaxSpeed);
            Serial.print("Increase speed to:");
            Serial.println(MaxSpeed);
        }
        else if (input == "m") {
            MaxSpeed -= 20;
            motor.setMaxSpeed(MaxSpeed);
            Serial.print("Decrease speed to:");
            Serial.println(MaxSpeed);
        }
        else if (input == "r") {
            swMode.sg_stop = 0;
            motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
            swMode.sg_stop = 1;
            motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
            Serial.println("Resetting Stallguard");
        }
        else if (input == "t") {
            MOTOR_TEST = !MOTOR_TEST;
            Serial.print("MOTOR_TEST:");
            Serial.println(MOTOR_TEST);
            if (!MOTOR_TEST) {
                // Stop motor
                motor.setTargetPosition(motor.getCurrentPosition());
                dir = !dir;
                t_dirchange = 0;
                t_echo = 0;
            }
        }
        else if (input == "c") {
            Serial.println("Initiate Homing");
            // motor.setAccelerations(25, 50, 100, 100);
            int d = 500;

            // homing
            // target negative position
            motor.setCurrentPosition(0);
            int temp_pos = -1000;

            int fast_stall_pos = move_until_stall(motor, drvStatus, temp_pos, MaxSpeed);
            Serial.print("fast_stall_pos: ");
            Serial.println(fast_stall_pos);
            Serial.println("Fast Zero position reached");

            // move motor back a bit
            temp_pos = fast_stall_pos + 10;
            motor.setTargetPosition(temp_pos);
            while (!motor.isTargetPositionReached()) {
                delay(d);
            }

            //// int slow_stall_pos = move_until_stall(motor, drvStatus, fast_stall_pos, VPWM+15);

            // Has stalled
            Serial.print("Motor Pos: ");
            Serial.println(motor.getCurrentPosition());
            Serial.println("Zero position reached");

            // reset position and target positive position
            motor.setCurrentPosition(0);
            temp_pos = 1000;

            fast_stall_pos = move_until_stall(motor, drvStatus, temp_pos, MaxSpeed);
            Serial.print("fast_stall_pos: ");
            Serial.println(fast_stall_pos);
            Serial.println("Fast Zero position reached");

            // move motor back a bit
            temp_pos = fast_stall_pos - 10;
            motor.setTargetPosition(temp_pos);
            while (!motor.isTargetPositionReached()) {
                delay(d);
            }

            //slow_stall_pos = move_until_stall(motor, drvStatus, fast_stall_pos, VPWM+15);
            // Has stalled again
            Serial.print("Motor Pos: ");
            Serial.println(motor.getCurrentPosition());
            Serial.println("End position reached");

            MaxPosition = (motor.getCurrentPosition());
            HOME_CAL = true;
            Serial.println("Homing complete");
            Serial.print("MaxPosition: ");
            Serial.println(MaxPosition);
            Serial.println("Moving to half position");

            // Move to middle position
            motor.setMaxSpeed(MaxSpeed);
            motor.setTargetPosition(0.5*MaxPosition);

            // reset stall
            delay(d);

            // reset accelerations and speeds

            motor.setAccelerations(1000, 1200, 2000, 2000);
            motor.setMaxSpeed(MaxSpeed);
            homed = true;
        }
        else {
            // Check longer command
            int index = input.indexOf(' ');
            if (index == -1) {
                // No space found
                Serial.print("Command '");
                Serial.print(input);
                Serial.println("' not found");
            }
            else {
                String command = input.substring(0, index);
                String params = input.substring(index + 1);
                if (command == "move") {
                    if (HOME_CAL) {
                        float percent_pos = params.toFloat();
                        float target = (percent_pos / 100.0f) * MaxPosition;
                        float prev_target = motor.getCurrentPosition();
                        motor.setTargetPosition(target);
                        Serial.print("Stepping to:");
                        Serial.println(target);
                        Serial.print("From:");
                        Serial.println(motor.getCurrentPosition());
                    }
                    else {
                        // Run the step command instead
                        Serial.println("Homing Calibration has not completed, running step command instead");
                        command == "step";
                    }
                }
                Serial.println(command);
                if (command == "step") {
                    int position = params.toInt();
                    motor.setTargetPosition(position);
                    Serial.print("Stepping to:");
                    Serial.println(position);
                    Serial.print("From:");
                    Serial.println(motor.getCurrentPosition());
                }
            }
        }
    }

    if (MOTOR_TEST) {
        // every n seconds or so...
        if (HOME_CAL) {
            if (motor.isTargetPositionReached()) {
                dir = !dir;
                motor.setTargetPosition(dir ? MaxPosition : 0);
            }
        }
        else {
            if (now - t_dirchange > dir_change_time || (now < dir_change_time && t_dirchange == 0)) {
                t_dirchange = now;

                // reverse direction
                dir = !dir;
                motor.setTargetPosition(dir ? 5000 : 0);  // 1 full rotation = 200s/rev
                // target += 16;
                // motor.writeRegister(TMC5160_Reg::XTARGET, target);
            }
        }
        VERBOSE = true;
    }
    else {
        VERBOSE = false;
    }

    if (VERBOSE) {
        // print out current position
        if (now - t_echo > print_time || (now < print_time && t_echo == 0)) {
            t_echo = now;

            // get the current target position
            float xactual = motor.getCurrentPosition();
            float vactual = motor.getCurrentSpeed();
            Serial.print("X:"); Serial.print(xactual/200*36, DEC); Serial.print(", ");
            Serial.print("V_rpm:"); Serial.print(vactual/200*60, DEC); Serial.print(", ");
            Serial.print("V_sps:"); Serial.print(vactual, DEC); Serial.print(", ");

            drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
            Serial.print("StallGuardValue:"); Serial.print(drvStatus.sg_result, DEC); Serial.print(", ");
            Serial.print("StallGuardStatus:"); Serial.print(drvStatus.stallguard ? true:false); Serial.print(", ");
            Serial.print("Actual_motor_current:"); Serial.print(drvStatus.cs_actual, DEC); Serial.print(", ");
            uint32_t tstep = motor.readRegister(TMC5160_Reg::TSTEP);
            float tstep_speed = 12000000.0/(256.0*tstep);
            Serial.print("tstep_speed:"); Serial.print(tstep_speed, DEC); Serial.print(", ");
            // Serial.print("TSTEP:"); Serial.print(motor.readRegister(TMC5160_Reg::TSTEP), DEC); Serial.print(", ");
            Serial.print("TPWMTHRS:"); Serial.print(motor.readRegister(TMC5160_Reg::TPWMTHRS), DEC); Serial.print(", ");
            Serial.print("TCOOLTHRS:"); Serial.print(motor.readRegister(TMC5160_Reg::TCOOLTHRS), DEC); Serial.print(", ");
            Serial.print("THIGH:"); Serial.print(motor.readRegister(TMC5160_Reg::THIGH), DEC); Serial.print(", ");

            rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
            Serial.print("position_reached:"); Serial.print(rampStatus.position_reached ? true : false); Serial.print(", ");
            Serial.println();
        }
    }
}
